abstract class BankCard(
    var balance: Int,
) {

    fun check(plusMoney: Int, next: () -> Unit) {
        if (plusMoney < 0 || balance < 0) {
            println("ERROR!!!")
        } else {
            next.invoke()
        }
    }

    abstract fun replenish(plusMoney: Int)
    abstract fun pay(plusMoney: Int): Boolean
    abstract fun balanceInfo()
    abstract fun getMoneyInfo()
}
