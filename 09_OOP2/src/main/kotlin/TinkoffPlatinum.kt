class TinkoffPlatinum(balance: Int, creditLimit: Int) : CreditCard(balance, creditLimit) {
    private var cashback = 0
    private var cashbackMoney = 0

    companion object {
        const val CASHBACK_PERCENT = 0.02
        const val ULTRA_CASHBACK_PERCENT = 0.06
        const val ULTRA_CASHBACK_BORDER = 5000
    }

    override fun pay(plusMoney: Int): Boolean {
        super.pay(plusMoney)
        if (plusMoney >= ULTRA_CASHBACK_BORDER) {
            cashback = (plusMoney * ULTRA_CASHBACK_PERCENT).toInt()
            balance += cashback
            cashbackMoney += cashback
        } else {
            println(plusMoney)
            cashback = (plusMoney * CASHBACK_PERCENT).toInt()
            balance += cashback
            cashbackMoney += cashback
        }
        return true
    }

    override fun getMoneyInfo() {
        super.getMoneyInfo()
        println("""
            Кэшбэк: $cashbackMoney
        """.trimIndent())
    }
}