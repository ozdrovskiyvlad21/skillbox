open class DebitCard(balance: Int) : BankCard(balance) {

    override fun replenish(plusMoney: Int) {
        check(plusMoney) {
            println("На какую сумму пополнить карту?")
            balance += plusMoney
        }
    }

    override fun pay(plusMoney: Int): Boolean {
        check(plusMoney) {
            println("Оплата на сумму")
            balance -= plusMoney
        }
        return true
    }

    override fun balanceInfo() {
        println("$balance")
    }

    override fun getMoneyInfo() {
    }
}