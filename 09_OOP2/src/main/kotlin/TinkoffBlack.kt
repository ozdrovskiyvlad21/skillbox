class TinkoffBlack(balance: Int) : DebitCard(balance) {
    private var cashback = 0
    private var cashbackMoney = 0

    companion object {
        const val BONUS = 0.005
        const val CASHBACK_PERCENT = 0.01
        const val ULTRA_CASHBACK_PERCENT = 0.05
        const val ULTRA_CASHBACK_BORDER = 5000
    }

    override fun replenish(plusMoney: Int) {
        check(plusMoney) {
            super.replenish(plusMoney)
            if (plusMoney > 0) {
                balance += (plusMoney * BONUS).toInt()
                cashbackMoney += (plusMoney * BONUS).toInt()

            }
        }
    }

    override fun pay(plusMoney: Int): Boolean {
        check(plusMoney) {
            super.pay(plusMoney)
            if (plusMoney >= ULTRA_CASHBACK_BORDER) {
                cashback = (plusMoney * ULTRA_CASHBACK_PERCENT).toInt()
                balance += cashback
                cashbackMoney += cashback
            } else {
                println(plusMoney)
                cashback = (plusMoney * CASHBACK_PERCENT).toInt()
                balance += cashback
                cashbackMoney += cashback
            }
        }
        return true
    }

    override fun getMoneyInfo() {
        println(
            """
         Деньги с кэшбэка: $cashbackMoney
         Баланс:           $balance
      """.trimIndent()
        )
    }
}
