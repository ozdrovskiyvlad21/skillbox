open class CreditCard(balance: Int, var creditLimit: Int) : BankCard(balance) {
    var creditDuty: Int = 20000
    override fun replenish(plusMoney: Int) {
        check(plusMoney) {
            println("Карта пополнена на сумму $plusMoney")
            if (creditDuty < creditLimit) {
                creditDuty += plusMoney
                if (creditDuty > creditLimit) {
                    val moreMoney = creditDuty - creditLimit
                    creditDuty = creditLimit
                    balance += moreMoney
                }
            } else balance += plusMoney
        }
    }

    override fun pay(plusMoney: Int): Boolean {
        check(plusMoney) {
            println("Оплата на сумму $plusMoney")
            if (balance < plusMoney) {
                balance -= plusMoney
                creditDuty += balance
                balance = 0
            } else {
                balance -= plusMoney
            }
        }
        return true
    }

    override fun balanceInfo() {
        println("$balance")
    }

    override fun getMoneyInfo() {
        print(
            """
         Кредитная карта с лимитом: $creditLimit
         Кредитные средства: $creditDuty
         Собственные средства: $balance
       
      """.trimIndent()
        )
    }
}
