import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flow

object Generator {
    val b = (1..90).shuffled().asFlow()

    suspend fun performRequest(request: Int): String {
        delay(1000)
        return "number $request"
    }
}