import kotlin.random.Random
import kotlin.random.nextInt

class Player {

    var list = generateList().toMutableList()

    private fun generateList(): MutableList<Int> {
        val array = mutableListOf<Int>()
        array.add((1..90).random())
        var b = 0

        for (i in 0 until 14) {
            b = (1..90).random()
            array.add(b)
        }

        if (hasDubls(array)) {
            do {
                replaseDubls(array)
            } while (hasDubls(array))
        }

        return array
    }

    fun changeToZero(index: Int): MutableList<Int> {
        list[index] = 0
        return list
    }

    private fun hasDubls(array: MutableList<Int>): Boolean {
        for (i in array) {
            for (j in i + 1 until array.size) {
                if (array[j] == array[i]) return true
            }
        }
        return false
    }

    private fun replaseDubls(array: MutableList<Int>): MutableList<Int> {
        for (i in array) {
            for (j in i + 1 until array.size) {
                if (array[j] == array[i]) array[i] = (1..90).random()
            }
        }
        return array
    }

    fun isWin(): Boolean {
        val line1 = list.slice(0..4).toMutableList()
        val line2 = list.slice(5..9).toMutableList()
        val line3 = list.slice(10..14).toMutableList()
        val x = List(5){0}
        return line1 == x || line2 == x || line3 == x
    }

    fun printAsCard(){
        val line1 = list.slice(0..4).toMutableList()
        val line2 = list.slice(5..9).toMutableList()
        val line3 = list.slice(10..14).toMutableList()


        println(line1)
        println(line2)
        println(line3)
    }
}