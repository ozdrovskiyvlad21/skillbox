import kotlinx.coroutines.*

fun main() {

    val player1 = Player()
    val player2 = Player()
    val player3 = Player()

    player1.printAsCard()
    println()
    player2.printAsCard()
    println()
    player3.printAsCard()
    println()

    val players = mutableListOf<Player>(player1, player2, player3)

    runBlocking {
        players.forEachIndexed() { i, it ->
           launch {
                Generator.b.collect { response ->
                    it.list.forEachIndexed() { index, arrayNumber ->
                        if (arrayNumber == response) {
                            it.changeToZero(index)
                            println("NUMBER $arrayNumber")
                            println("Player${i+1} card:")
                            it.printAsCard()
                            println()
                        }
                    }
                    if(it.isWin()) {
                        println("Player${i+1} is WINNER")
                        cancel()
                    }
                    delay(800)
                }
            }
        }
    }


}




