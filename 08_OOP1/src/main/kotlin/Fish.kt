open class Fish(name: String, energy: Int, weight: Int, maxAge: Int) :
    Animal(name, maxAge, energy, weight) {

    override fun move(): Boolean {
        if (super.move())
            println("плывёт")
        return true
    }

    override fun born(): Fish {
        val newFish =
            Fish(name = name, maxAge = maxAge, energy = (1..10).random(), weight = (1..5).random())
        println("${newFish.name} родился с характеристиками:\n" +
                "energy ${newFish.energy}\n" +
                "weight ${newFish.weight}")
        return newFish
    }

}
