open class Dog(name: String, energy: Int, weight: Int, maxAge: Int) :
    Animal(name,  maxAge, energy, weight) {

    override fun move(): Boolean {
        if (super.move())
            println("бежит")
        return true
    }

     override fun born(): Dog {
        val newDog =
            Dog(name = name, maxAge = maxAge, energy = (1..10).random(), weight = (1..5).random())
        println("${newDog.name} родился с характеристиками:\n" +
                "energy ${newDog.energy}\n" +
                "weight ${newDog.weight}")
        return newDog
    }

}
