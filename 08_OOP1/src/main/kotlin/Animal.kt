
import kotlin.random.Random
open class Animal(
    val name: String,
    val maxAge: Int,
    var energy: Int,
    var weight: Int,
) {

    var age: Int = 1
    val isTooOld: Boolean
        get() = age >= maxAge


    fun sleep() {
        if(checkToMove()) {
            energy += 5
            age += 1
            println("$name спит")
        }
    }

    fun eat() {
        if(checkToMove()) {
            energy += 3
            weight += 1
            tryIncrementAge()
            println("$name ест")
        }
    }

    private fun tryIncrementAge() {
        if (Random.nextBoolean()) {
            age += 1
        }
    }

    open fun move(): Boolean{
        return if(checkToMove()){
            energy -= 5
            weight -= 1
            tryIncrementAge()
            println("$name передвигается")
            true
        } else{
            println("$name Не может двигаться")
            false
        }
    }

    open fun checkToMove(): Boolean{
        return !(energy<=0 || isTooOld || weight<=0)
    }

    open fun born(): Animal {
        val newAnimal =
            Animal(name = name, maxAge = maxAge, energy = (1..10).random(), weight = (1..5).random())
        println("${newAnimal.name} родился с характеристиками:\n" +
                "energy ${newAnimal.energy}\n" +
                "weight ${newAnimal.weight}")
        return newAnimal
    }

}