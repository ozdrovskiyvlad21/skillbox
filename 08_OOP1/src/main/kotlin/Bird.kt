open class Bird(name: String, energy: Int, weight: Int, maxAge: Int) :
    Animal(name, maxAge, energy, weight) {

    override fun move(): Boolean {
        if (super.move())
            println("летит")
        return true
    }

     override fun born(): Bird {
        val newBird =
            Bird(name = name, maxAge = maxAge, energy = (1..10).random(), weight = (1..5).random())
        println("${newBird.name} родился с характеристиками:\n" +
                "energy ${newBird.energy}\n" +
                "weight ${newBird.weight}")
        return newBird
    }

}