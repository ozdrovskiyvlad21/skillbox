public class NatureReserve {
    val eagle = Bird("Орёл",  maxAge = 30, energy = 20, weight = 5)
    val hawk = Bird("Ястреб",  maxAge = 30, energy = 20, weight = 5)
    val swallowBird = Bird("Ласточка",  maxAge = 6, energy = 5, weight = 2)
    val crane = Bird("Журавль",  maxAge = 10, energy = 5, weight = 5)
    val penguin = Bird("Пингвин",  maxAge = 10, energy = 5, weight = 5)

    val catfish = Fish("Сом",  maxAge = 6, energy = 10, weight = 12)
    val shark = Fish("акула",  maxAge = 50, energy = 50, weight = 50)
    val ramp = Fish("Скат ",  maxAge = 5, energy = 5, weight = 10)

    val poodle = Dog("Пудель",  maxAge = 30, energy = 80, weight = 30)
    val labrador = Dog("Лабрадор",  maxAge = 30, energy = 100, weight = 50)

    val antelope = Animal("Антилопа",  maxAge = 40, energy = 100, weight = 200)
    val crocodile = Animal("Крокодил",  maxAge = 150, energy = 75, weight = 500)
    val anaconda = Animal("Анаконда",  maxAge = 100, energy = 35, weight = 200)


    var natureReserve = mutableListOf(
        eagle,
        hawk,
        swallowBird,
        crane,
        penguin,
        catfish,
        shark,
        ramp,
        poodle,
        labrador,
        antelope,
        crocodile,
        anaconda)

    fun print() {
        for (i in 0 until natureReserve.size) {
            println(natureReserve[i].name + "    Характеристики: " +
                    "Возраст " + natureReserve[i].age +
                    ", Запас энергии- " + natureReserve[i].energy +
                    ", Вес- " + natureReserve[i].weight +
                    ", Умрёт в " + natureReserve[i].weight)
        }
    }

    fun natureCycle(){
        for (animal in natureReserve.indices) {
            natureReserve[animal].move()
            natureReserve[animal].eat()
            natureReserve[animal].sleep()
           // natureReserve.add(natureReserve[animal].born())
        }
        die()
    }

    fun die(){
        natureReserve = natureReserve.filter { !it.checkToMove() }.toMutableList()
    }

}