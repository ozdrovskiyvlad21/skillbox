import kotlinx.coroutines.*
import java.math.BigInteger

val exeptionHandler = CoroutineExceptionHandler { _, throwable ->
    println("error ${throwable.message}")
}

fun main() {

    runBlocking {
        val job = launch(exeptionHandler) {
            try {
                var i = 1
                var maxValue = 2000
                var n: BigInteger = i.toBigInteger()
                withTimeout(5000) {
                    while (n.toInt() < 2000) {
                        n = Fibonacci.take(i)
                        println(n)
                        i++
                       delay(700)
                    }
                }
            } catch (e: Throwable) {
                println("Calculation was too long")
            }
        }
    }
}

object Fibonacci {
    suspend fun take(i: Int): BigInteger {
        var count1 = 1
        var count2 = 1
        for (i in 3..i) {
            yield()
            var next = count1
            count1 += count2
            count2 = next
        }
        return count1.toBigInteger()
    }
}