sealed class BattleState(val progress: String) {
    class Progress(progress: String, team1:Team, team2: Team) : BattleState(progress) {
        val aliveWarriorsInTeam1: Int = team1.teamSoldiers.size
        val sumHPInTeam1 = team1.teamSoldiers.forEach{it.HP++}

        val aliveWarriorsInTeam2: Int = team2.teamSoldiers.size
        val sumHPInTeam2 = team2.teamSoldiers.forEach{it.HP++}
    }

    object FirstTeamWin: BattleState("1 team win")
    object SecondTeamWin: BattleState("2 team win")
    object Standoff: BattleState("standoff")

}