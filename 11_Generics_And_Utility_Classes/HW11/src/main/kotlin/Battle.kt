import java.util.Random

class Battle(val team1: Team, val team2: Team, EndBattle: Boolean) {

    private fun getProgress(): BattleState {
        return if (team1.teamSoldiers.isEmpty()) BattleState.SecondTeamWin
        else if(team2.teamSoldiers.isEmpty()) BattleState.FirstTeamWin
        else if(team1.teamSoldiers.isEmpty() && team2.teamSoldiers.isEmpty()) BattleState.Standoff
        else BattleState.Progress("Teams still fighting", team1, team2)
    }

    fun getBattle() {
            team1.teamSoldiers.forEach {
                if (!it.isKilled) it.goAttacked(team2.teamSoldiers[(0 until team2.teamSoldiers.size).random()])
            }
            team2.teamSoldiers.forEach {
                if (!it.isKilled) it.goAttacked(team1.teamSoldiers[(0 until team1.teamSoldiers.size).random()])
            }
        getProgress()
    }
}