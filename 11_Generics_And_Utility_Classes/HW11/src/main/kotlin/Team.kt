class Team(private val countOfWarriors: Int) {

    val teamSoldiers: List<AbstractWarrior> = createTeam()

    private fun createTeam(): List<AbstractWarrior> {
        val team = Stack<AbstractWarrior>()

        do {

            createUnit()

        } while (team.list.size != countOfWarriors && countOfWarriors != 0)

        for(i in 0 until team.list.size){
            if(team.list[i].isKilled) team.list.removeAt(i)
        }

        return team.list
    }

    private fun createUnit(): AbstractWarrior {

        val newUnit = when ((1..100).random()) {

            in 1..50 -> Soldier(false, 20)

            in 51..87 -> Captain(false, 50)

            in 88..100 -> General(false, 70)

            else -> Soldier(false, 20)
        }
        return newUnit
    }


}