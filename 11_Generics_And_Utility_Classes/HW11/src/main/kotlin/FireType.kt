sealed class FireType{
    object SingleShot: FireType()
    data class BurstShooting(val int: Int): FireType()
}
