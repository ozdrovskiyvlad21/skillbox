interface Warrior {
    var isKilled: Boolean
    var chanceToNotBeShoot: Int
    fun goAttacked(warrior: Warrior)
    fun getDamage(damage: Double)
}