enum class Ammo(val damage: Int, val chanceCrit: Int, val coefficientCrit: Double) {
    F32(10, 50, 1.2),
    S75(80, 20, 2.0),
    M45(30, 30, 1.5);

    open fun damageReal(): Double {
        return if (chanceCrit.chanceCome()) damage * coefficientCrit
        else damage.toDouble()
    }
}