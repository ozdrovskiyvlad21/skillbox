import kotlin.random.Random

fun Int.chanceCome(): Boolean {
    val random = Random.nextInt(100 / this)
    if (this >= 100) return true
    return random == 0
}

fun main() {
    val object1 = Stack<Int>()
    object1.push(5)
    println(object1.pop())

    fun print() {
        println(object1.list.joinToString(","))
    }

}