object Weapons {

    fun createPistol(): AbstractWeapon {
        return object : AbstractWeapon(10, FireType.SingleShot, Stack(), false){
            override fun createAmmo(): Ammo {
                return  Ammo.M45
            }
        }
    }

    fun createMiniGun(): AbstractWeapon{
        return object : AbstractWeapon(200, FireType.SingleShot, Stack(), false){
            override fun createAmmo(): Ammo {
                return Ammo.F32
            }
        }
    }

    fun createSniper(): AbstractWeapon{
        return object : AbstractWeapon(5, FireType.SingleShot, Stack(), false){
            override fun createAmmo(): Ammo {
                return Ammo.S75
            }
        }
    }

    fun createBurstGun(): AbstractWeapon{
        return object : AbstractWeapon(30, FireType.BurstShooting(3), Stack(), false){
            override fun createAmmo(): Ammo {
              return Ammo.F32
            }

        }
    }
}