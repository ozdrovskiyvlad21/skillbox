abstract class AbstractWarrior(
    val MaxHP: Int,
    val deviation: Int,
    val accuracy: Int,
    val weapon: AbstractWeapon,
    var HP: Int
) : Warrior {
    override fun goAttacked(warrior: Warrior) {
        if (weapon.EmptyMagazine) {
            weapon.reloading()
            //TODO пропустить ход
        } else {
            var sumDamage = 0.0
            repeat(weapon.magazine.list.size) {
                val ammo = weapon.createAmmo()
                if (accuracy.chanceCome() && !warrior.chanceToNotBeShoot.chanceCome()) sumDamage =
                    ammo.damageReal() * weapon.magazine.list.size
            }
            warrior.getDamage(sumDamage)
        }
    }

    override fun getDamage(damage: Double) {
        HP - damage
        if(HP<=0) isKilled
        else !isKilled
    }
}