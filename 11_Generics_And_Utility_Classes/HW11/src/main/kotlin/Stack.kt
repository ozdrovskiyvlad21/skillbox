open class Stack<T> {
    open var list : MutableList<T> = mutableListOf()

    fun push(item: T) {
        list.add(item)
    }

    fun pop(): T? {
        return if(list.size != 0) list.removeAt(list.size-1)
        else null
    }

    fun isEmpty(): Boolean {
        return list.size == 0
    }
}