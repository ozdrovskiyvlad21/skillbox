import kotlin.properties.Delegates

abstract class AbstractWeapon(
    val maxMagazine: Int,
    val fireType: FireType,
    var magazine: Stack<Ammo>,
    var EmptyMagazine: Boolean
) {

    abstract fun createAmmo() : Ammo

    fun reloading(): Stack<Ammo> {
        val magazine2 =  Stack<Ammo>()
        do magazine2.push(createAmmo())
        while (magazine2.list.size != maxMagazine)
        magazine = magazine2
        return magazine
    }

    fun fireGun(): List<Ammo> {
        val ammoForShot: List<Ammo> = listOf()
        val x by Delegates.notNull<Int>()
        if (fireType == FireType.SingleShot) {
                ammoForShot.plus(magazine.pop())
        }
        else if (fireType == FireType.BurstShooting(x)) {
            do ammoForShot.plus(magazine.pop())
                while (magazine.list.size != magazine.list.size-x)
        }
        return ammoForShot
    }

}