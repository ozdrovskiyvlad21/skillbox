interface CurrencyConverter {
    val currencyCode: String
   open fun convertToRub(n: Int)
}