object Converters {
    private val converterMap: Map<String, CurrencyConverter> = mapOf("USD" to USD(), "EUR" to EUR(), "GBR" to GBP())

    fun get(currencyCode: String): CurrencyConverter? {
       val anonymous = object : CurrencyConverter {
            override val currencyCode: String = currencyCode
                override fun convertToRub(n: Int) {
                println("Indicate the price of the currency $currencyCode")
                val m = readLine()?.toInt() ?: 0
                println("$n $currencyCode = ${m * n} RUB")
            }
        }
        return if(converterMap.containsKey(currencyCode)) converterMap[currencyCode]
        else anonymous

    }

}