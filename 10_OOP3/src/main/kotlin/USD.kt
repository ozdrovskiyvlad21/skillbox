import kotlin.random.Random

class USD() : CurrencyConverter {
    override val currencyCode = "USD"
    override fun convertToRub(n:Int) {
        println("$n USD = ${71*n} RUB")
    }
}