import kotlin.random.Random

class GBP() : CurrencyConverter {
    override val currencyCode = "GBR"
    override fun convertToRub(n:Int) {
        println("$n GBR = ${97*n} RUB")
    }
}