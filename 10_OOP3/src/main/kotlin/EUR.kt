import kotlin.random.Random

class EUR() : CurrencyConverter {
    override val currencyCode = "EUR"
    override fun convertToRub(n:Int) {
        println("$n EUR = ${81*n} RUB")
    }

}
